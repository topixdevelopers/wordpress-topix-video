# Top-ix Video PLayer
Questo è il plugin wordpress per embeddare i video forniti da TOP-IX nella vostra pagina.

## Installazione: 
   - entrare nella sezione "Plugins" del vostro WordPress manager
   - cliccare su "Aggiungi nuovo" -> "Carica plugin"
   - scegliere il file zip che vi abbiamo fornito
   - ritornare alla pagina dei plugin, trovare il plugin chiamato "TOP-IX Video Player" e cliccare attiva

## Utilizzo:
   - nella pagina dove vogliamo inserire il video inserire il codice [topix-video app="<nome_app>" player="<nome_del_player>"]
   - sostituire <nome_app> con il nome del mountpoint che vi abbiamo assegnato
   - sostituire <nome_del_player> con il nome del player da inserire nella pagina
### E' possibile inoltre aggiungere altri campi opzionali:
   - width="<width>" height="<height>" per regolare la grandezza dell'iframe contentente il player (es. width="100%" oppure width="640px")
   - background="<image_url>" per personalizzare l'immagine di sfondo
   - streamname="<streamname>" per definire uno specifico nome di stream live o nome del file on-demand da visualizzare nel player