<?php
/*
Plugin Name: TOP-IX Video Player
Version: 0.0.1
Plugin URI: 
Author: SimoneDutto
Author URI: simone.dutto@top-ix.org
Description: Easily embed top-ix video into your WordPress blog
Text Domain: topix-video-player
 */
add_shortcode( 'topix-video', 'topixvideo_handler');

function topixvideo_handler($atts){
    $attr = shortcode_atts( array(
        'app' => '',
        'player' => '',
        'streamname' => '',
        'width' => '',
        'height' => '',
        'background' => ''
        ), $atts );
    $attr = array_map('sanitize_text_field', $attr);
    extract($attr);

    $errors=[];
    $output="";

    if (! isset($app) || $app == "") {
        $errors[] = "\"app\" parameter is not defined";
    }
    if (! isset($player) || $player == "") {
        $errors[] = "\"player\" parameter is not defined";
    }
    if ($height == ''){
        $height = "180px";
    }
    if ($width == ''){
        $width = "320px";
    }

    if (count($errors) > 0) {
        $output = "<div style=\"width: " . $width . "; height: " . $height . "; background-color: lightgrey; color:gray\">" . implode("<br/>", $errors) . "</div>";
    } else {
        $url = "https://live.top-ix.org/play/".$app."/".$player;
        $url_args=[];
        if ($streamname != ""){
            $url_args[] = "streamname=".$streamname;
        }
        if ($background != ""){
            $url_args[] = "background=".$background;
        }
        $url = $url."?".implode("&", $url_args);

        $output = <<<EOT
    <iframe height=$height width=$width style="border:None" 
    src="$url" allowfullscreen scrolling="no"></iframe>
EOT;
    }
    return $output;
}
add_action('init', 'topixvideo_handler');
?>
